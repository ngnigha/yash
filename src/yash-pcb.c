#include "yash-pcb.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


taskDescription_t *newTask(int pid, char **taskBegin)
{
    taskDescription_t *task = (taskDescription_t*) malloc( sizeof(taskDescription_t) );

    task->pid = pid;
    task->gpid = -1;
    task->cmdAndArgs = taskBegin;
    task->redirectStdoutToPipe = FALSE;
    task->redirectStdinToPipe  = FALSE;
    task->stdinFileRedirect  = FALSE;
    task->stdoutFileRedirect = FALSE;
    task->stderrFileRedirect = FALSE;
    task->stdinFile  = NULL;
    task->stdoutFile = NULL;
    task->stderrFile = NULL;
    task->next = NULL;

    return task;
}

jobDescription_t *newJob(int id, const char *cmd)
{
    jobDescription_t *job = (jobDescription_t*) malloc( sizeof(jobDescription_t) );
    job->id = id;
    job->cmd = strdup(cmd);
    job->status = READY;
    job->isBackground = FALSE;
    job->lastUpdate = 0;
    job->taskList = NULL;
    job->next = NULL;

    char *p = strchr(job->cmd, '\n');
    if(p)
    {
        *p = 0;
    }
    return job;
}

int add_job_to_pcb( pcb_t *pcb, jobDescription_t *job )
{
    // Most recently added job first
    if( job == NULL )
    {
        return -1;
    }

    if( pcb->jobList == NULL )
    {
        job->id = 0;
        pcb->jobList = job;
    }
    else
    {
        job->id = pcb->jobList->id + 1;
        job->next = pcb->jobList;
        pcb->jobList = job;
    }

    return 0;
}

int add_task_to_job( jobDescription_t *job, taskDescription_t *task )
{
    // Most recently added task last
    if( task == NULL || job == NULL )
    {
        return -1;
    }

    task->gpid = job->id;

    if( job->taskList == NULL )
    {
        job->taskList = task;
    }
    else
    {
        taskDescription_t *cur = job->taskList;

        while( cur->next != NULL )
        {
            cur = cur->next;
        }

        cur->next = task;
    }

    return 0;
}

void destroy_task(taskDescription_t *task)
{
    free(task);
}

void destroy_job( jobDescription_t *job )
{
    if( job ==  NULL )
    {
        return;
    }

    while(job->taskList)
    {
        taskDescription_t *t = job->taskList;
        job->taskList = job->taskList->next;
        destroy_task(t);
    }

    free(job->cmd);
    free(job);
}

void destroy_pcb( pcb_t *pcb )
{
    if( pcb ==  NULL )
    {
        return;
    }

    while(pcb->jobList)
    {
        jobDescription_t *j = pcb->jobList;
;
        pcb->jobList = pcb->jobList->next;
        destroy_job(j);
    }
}


void yash_update_to_background(jobDescription_t *job, bool_t bg, int utime)
{
    if(job)
    {
        job->lastUpdate = utime;
        job->isBackground = bg;
    }
}

void delete_job(pcb_t *pcb, jobDescription_t *job)
{
    if(!job)
    {
        return;
    }

    if(pcb->jobList == job)
    {
        pcb->jobList = job->next;
    }
    else
    {
        jobDescription_t *cur = pcb->jobList;
        while(cur)
        {
            if(cur->next == job)
            {
                cur->next = job->next;
                break;
            }
            cur = cur->next;
        }
    }

    destroy_job(job);
}
