#ifndef YASH_JOB_CONTROL_H
#define YASH_JOB_CONTROL_H

typedef enum
{
    JOBS = 0,
    FOREGROUND,
    BACKGROUND,
    NEWLINE,
    FORKJOB
} job_type_t;

job_type_t yash_get_job_type(const char * cmd);
void yash_process_control_job(const job_type_t type);


#endif // YASH_JOB_CONTROL_H
