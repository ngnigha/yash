#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "yash-pcb.h"
#include "yash-processing.h"
#include "yash-signals.h"
#include "yash-job-control.h"

extern pcb_t processController = {0, NULL, NULL};

int read_command_input( char *command )
{
    size_t sz = MAX_LINE_SIZE;
    printf( "# " );
    size_t bytes = getline( &command, &sz, stdin);

    return bytes;
}

int process_command( pcb_t *pcb )
{
    char * tokens[MAX_LINE_SIZE];
    char *cmd = strdup(pcb->jobList->cmd);

    // Tokenize input
    const char delim[] = " ";
    char *tok = strtok( cmd, delim);

    char **iter = tokens;
    while( tok != NULL )
    {
        char *p = strchr(tok, '\n');
        if(p)
        {
            *p = 0;
        }

        *iter = tok;
        tok = strtok(NULL, delim);
        iter++;
    }
    *iter = tok;

    execute_job( pcb, tokens);
    free(cmd);
    return 0;
}


int main(int argc, char ** argv)
{
    setbuf(stdout, NULL);
    char cmd[MAX_LINE_SIZE];

    signal(SIGINT, yash_sigint_handler);
    signal(SIGTSTP, yash_sigtstp_handler);
    signal(SIGCHLD, yash_sigchld_handler);

    while( 1 )
    {
        job_type_t jobType;
        int b = read_command_input(cmd);

        if( b == -1 )
        {
            // Exit this session
            yash_clean_exit();
        }
        else if( (jobType = yash_get_job_type(cmd)) == FORKJOB )
        {
            jobDescription_t *job = newJob(-1, cmd);
            add_job_to_pcb(&processController, job);
            process_command( &processController );
        }
        else
        {
            yash_process_control_job(jobType);
        }
    }

    return 0;
}
