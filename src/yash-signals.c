#include "yash-signals.h"
#include "yash-processing.h"
#include <unistd.h>
#include <sys/wait.h>


extern pcb_t processController;

void yash_send_signal(jobDescription_t *job, int signum)
{
    if(!job)
    {
        return;
    }

    taskDescription_t *task = job->taskList;
    while(task)
    {
        kill(task->pid, signum);
        task = task->next;
    }
}

void yash_sigint_handler(int signum)
{
    jobDescription_t * job = processController.fgJob;

    yash_send_signal(job, signum);
    if(job)
    {
        job->status = DONE;
    }

    // delete job
    delete_job(&processController, job);
    processController.fgJob = NULL;
}

void yash_sigtstp_handler(int signum)
{
    jobDescription_t *job = processController.fgJob;

    yash_send_signal(job, signum);
    if(job)
    {
        job->status = STOPPED;
    }
    processController.fgJob = NULL;
}

void yash_sigchld_handler(int signum)
{
    jobDescription_t *job = processController.jobList;

    while(job)
    {

        bool_t check = TRUE;
        taskDescription_t *task = job->taskList;
        while (task)
        {
            int exitStatus;
            if(job == processController.fgJob)
            {
                waitpid(task->pid, &exitStatus, WUNTRACED);
                processController.fgJob = NULL;
            }

            pid_t retPid = waitpid(task->pid, &exitStatus, WNOHANG);
            if(retPid == 0)
            {
                check = check? FALSE : check;
            }
            task = task->next;
        }
        jobDescription_t * tp = job;
        job = job->next;

        if(check)
        {
            if( tp->isBackground )
            {
                tp->status = DONE;
            }
            else
            {
                delete_job(&processController, tp);
            }
        }
    }
}

void yash_clean_exit(void)
{
    jobDescription_t *job = processController.jobList;

    while(job)
    {
        yash_send_signal(job, SIGTERM);
        job = job->next;
    }

    destroy_pcb(&processController);
    exit(0);
}
