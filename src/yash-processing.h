#ifndef YASH_PROCESSING_H
#define YASH_PROCESSING_H

#include "yash-pcb.h"

pid_t execute_job( pcb_t *pcb, char **commandArgs );

pid_t simple_command_fork( char *const commandArgs[], const bool_t pipeMode );

pid_t pipe_tasks( char *const commandArgs[], const bool_t pipeMode );

pid_t output_redirection_fork( char *const commandArgs[], const bool_t pipeMode );

pid_t yash_wait_job(jobDescription_t *job);

#endif // YASH_PROCESSING_H
