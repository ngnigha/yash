#ifndef YASH_PCB_H
#define YASH_PCB_H

#include <sys/types.h>

#define INPUT_REDIRECTION   "<"
#define OUTPUT_REDIRECTION  ">"
#define PIPE_OPERATION      "|"
#define BACKGROUND_JOB     "&"
#define MAX_LINE_SIZE       200

#define READ                0
#define WRITE               1

typedef enum _bool_t
{
    TRUE = 1,
    FALSE = 0
}bool_t;

typedef enum _redirect_mode_t
{
    STDIN  = 0,
    STDOUT = 1,
    STDERR = 2,
    NONE
} redirect_mode_t;

typedef enum _status_t
{
    READY = 0,
    RUNNING,
    STOPPED,
    DONE
}status_t;

typedef struct _task_description_t
{
    int pid;
    int gpid;
    char **cmdAndArgs;

    int pipes[2];
    bool_t redirectStdinToPipe;
    bool_t redirectStdoutToPipe;

    bool_t stdinFileRedirect;
    bool_t stdoutFileRedirect;
    bool_t stderrFileRedirect;
    char *stdinFile;
    char *stdoutFile;
    char *stderrFile;

    struct _task_description_t *next;
}taskDescription_t;


typedef struct _job_description_t
{
    int id;
    char *cmd;
    status_t status;
    bool_t isBackground;

    int lastUpdate;

    taskDescription_t *taskList;
    struct _job_description_t * next;
} jobDescription_t;


typedef struct _pcb_t
{
    int LastUpdate;
    jobDescription_t *fgJob;
    jobDescription_t *jobList;
}pcb_t;


int add_job_to_pcb( pcb_t *pcb, jobDescription_t *job );
int add_task_to_job( jobDescription_t *job, taskDescription_t *task );

jobDescription_t  *newJob( int pid, const char *cmd );
taskDescription_t *newTask( int id, char **taskBegin );

void destroy_task(taskDescription_t *task);
void destroy_job( jobDescription_t *job );
void destroy_pcb( pcb_t *pcb );

void delete_job(pcb_t *pcb, jobDescription_t *job);

void yash_update_to_background(jobDescription_t *job, bool_t bg, int utime);

extern pcb_t processController;

#endif // YASH_PCB_H
